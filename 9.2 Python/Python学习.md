# 1 学习准备

## 1.1 安装Python环境

- [windows系统中python3.10安装简易教程 - 简书 (jianshu.com)](https://www.jianshu.com/p/e14a8cce863f)

## 1.2 学习Markdown（方便学习，记笔记等）

- [typora下载和破解（仅供学习） - hackettt - 博客园 (cnblogs.com)](https://www.cnblogs.com/hackettt/p/16335288.html)

- [Typora 1.4.8 – 2022最新Typora破解激活教程！你的Markdown编辑器！ - 郭炫韩Coding - 博客园 (cnblogs.com)](https://www.cnblogs.com/guoxuanhan/p/16841068.html)
- [Markdown 教程 | 菜鸟教程 (runoob.com)](https://www.runoob.com/markdown/md-tutorial.html)

## 1.3 学习Git

- [Windows安装Git通俗易懂](https://cloud.tencent.com/developer/article/2134190)

# 2 教程

## 2.1 Python教程（可以先看这部分，基础的先看看熟悉熟悉）

- [Python3 教程 | 菜鸟教程 (runoob.com)](https://www.runoob.com/python3/python3-tutorial.html) 

- [Python数据科学 (yiibai.com)](https://www.yiibai.com/python_data_science)
- [Python文本处理教程 (yiibai.com)](https://www.yiibai.com/python_text_processing/)
- [Python数据结构 (yiibai.com)](https://www.yiibai.com/python/py_data_structure/)



- https://github.com/jackfrued/Python-100-Days   （可能打不开，多刷新几次，就可以打开了）

思维导图Python：

- [大家分享的文件 | ProcessOn](https://www.processon.com/popular?criterion=python)

## 2.2 MySQL数据库（基础知识需要了解）

- [MySQL教程 (yiibai.com)](https://www.yiibai.com/mysql/)

## 2.3 B站还可以的Python教程（可以先看这个，找一个看看，之后在针对性学习）

- [【整整600集】顶级大学196小时讲完的Python教程](https://www.bilibili.com/video/BV1rN4y1F7AU/?spm_id_from=333.337.search-card.all.click&vd_source=48ad63b9f2cfd2ad813d4fea3aaaf77b)