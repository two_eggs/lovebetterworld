- [数据中台(安全篇) - 一寸HUI - 博客园 (cnblogs.com)](https://www.cnblogs.com/zsql/p/15801868.html)

## 数据安全的挑战

- 企业内部挑战：从企业内部来说，一方面，大数据平台的安全管控能力缺失，使得平台在数据存储、处理以及使用等各环节造成数据泄露的风险较大，安全风险面广，且缺乏有效的处理机制；另一方面，企业敏感数据的所有权和使用权缺乏明确界定和管理，可能造成用户隐私信息和企业内部数据的泄露，直接造成企业声誉和经济的双重损失。
- 对大数据服务商的挑战：随着企业云化进程的加速，越来越多的数据会产生、处理、存储在云端，不论是公有云还是私有云都需要支撑各类技术服务商，站在大数据服务提供商的角度，在服务客户的过程中，也越来越能感受到客户的这种变化和要求。在数据中台的项目中，客户要求在存储、传输、使用、共享个人信息的阶段，均需要去标识化。如进行身份证号隐藏、家庭住址隐藏等。
- 数据确权问题：数据的所有权、使用权、管理权涉及个人、企业、政府和其他组织，一旦处理不当，会损害个人隐私、组织利益、国家安全等。在进行大数据收集、处理和应用的过程中，必须做到权责分明，厘清数据权属关系，防止数据流通过程中的非法使用，保障数据安全流通。

## 数据安全管理



### 1.安全问题出现在哪里

数据安全管理，重点放在大数据平台的安全管理技术手段上。数据安全管理既是数据资产管理中不可或缺的一部分，又是
信息安全管理的重要组成部分。数据安全除了在**数据平台安全，服务安全，数据本身安全**，还会在数据的各个阶段存在着风险，我们先看看数据的**生命周期**，下图所示：
![img](https://img2020.cnblogs.com/blog/1271254/202201/1271254-20220114144544412-848127800.png)

- 数据产生： 指新的数据产生或现有数据内容发生显著改变或更新的阶段。
- 数据存储： 指非动态数据以任何数字格式进行物理存储的阶段。
- 数据传输： 指数据在组织内部从一个实体通过网络流动到另一个实体的过程。
- 数据使用： 指组织在内部针对动态数据进行的一系列活动的组合。
- 数据共享： 指数据经由组织与外部组织及个人产生交互的阶段。
- 数据销毁： 指利用物理或者技术手段使数据永久或临时不可用的过程。

数据生命周期的每一环节上基于不同类型的数据、不同的应用系统、不同的人员等有不同的风险，无论哪一个环节出现了问题，都有可能发生数据安全事件。这很容易理解，只要出现一个薄弱环节，敌人一定会首先从那里发起攻击。数据的价值与日俱增，靠窃取数据获取非法收入的黑灰色产业链给数据安全防护带来巨大风险。



### 2.安全从哪些方面建设

数据安全管理工具是结合信息安全的技术手段保证数据资产使用和交换共享过程中的安全。数据管理人员开展数据安全管理，是指执行数据安全政策和措施，为数据和信息提供适当的认证、授权、访问和审计，以防范可能的数据安全隐患。可以从如下方面进行对数据安全进行建设：

- **数据获取安全**：能够支持数据获取需要经过申请与审批流程，保障数据获取安全；
- **数据脱敏**：能够支持数据脱敏规则、脱敏算法及脱敏任务的管理及应用，一般情况下，脱敏方式有动态脱敏和静态脱敏两种；
- **统一认证**：定义数据安全策略，定义用户组设立和密码标准等；
- **租户隔离**：管理用户，密码，用户组和权限；
- **角色授权**：划分信息等级，使用密级分类模式，对企业数据和信息产品进行分类；
- **日志审计**：审计数据安全，监控用户身份认证和访问行为，支持经常性分析；
- **异常监控**：指对账号异常行为的监控，如同一账号异地登录、同时多 IP 登录、多次重复登录等；
- **数据分类分级**：能够支持对数据资产安全进行敏感分级管理，并支持根据各级别生成对应的数据安全策略。



### 3.安全建设的战略是什么

整体的数据安全管理体系通过分层建设、分级防护，利用平台能力及应用的可成长、可扩充性，创造面向数据的安全管理体系系统框架，形成完整的数据安全管理体系。数据中台的建设，应该始终把数据安全管理放在最重要的位置上，通过设计完备的数据安全管理体系，多方面、多层次保障大数据安全。一个完备的数据安全管理体系包括**安全战略、安全组织管理、安全过程管理、安全技术保障、数据运行能力保障、数据生命周期安全保障**。如下图所示：

![img](https://img2020.cnblogs.com/blog/1271254/202201/1271254-20220114144613357-1418101173.png)

- **安全战略层面**：企业的主要负责人要带头深入理解业务范围内世界各国对于数据安全与隐私相关的法律法规，制定适合企业可落地的相关制度，并进行组织规划。
- 安全组织管理层面：要建设相关的数据安全保障组织，开展人才储备、宣传培训等工作，并保证相关的资源到位。
- **安全过程管理层面**：需要设计一套涵盖规划、设计、实施、运维、测评、改进的安全管控流程，通过流程的不断循环，持续改善安全管理各个环节的水平。
- **安全技术保障层面**：要从系统层安全、应用层安全、数据层安全、平台设施层安全等多个层次保障安全。以系统层安全为例，需要选用高安全性、成熟的操作系统，从官方渠道下载和打补丁，保障安全扫描软件的正常运行等。
- **数据生命周期安全保障层面**：要根据数据在生命周期的不同阶段，设计不同的安全防护措施，以数据传输安全为例，要保证数据传输的安全，保证敏感数据传输的时候不会被截取，需要传输加密等手段，即使黑客截获了数据包，也无法解析其中的内容。
- **数据运行能力保障层面**：需要态势感知、监控预警、阻断和恢复等多种手段，举例来说，大数据平台可以识别和监控可疑账户，一旦可疑账户发生异常访问，如访问敏感数据，或者频繁查询和获取某些数据，系统可以立刻发出告警，并阻断和跟踪该账户的其他网络行为。



### 4.安全架构和安全手段

#### 1.安全架构

数据安全架构主要从六个方面考虑，包括**物理安全、系统安全、网络安全、应用安全、数据安全和管理安全**六个维度。如下图所示：数据中台的安全主要关注的是应用安全（平台）以及数据安全，其他的偏底层硬件，系统方面。

![img](https://img2020.cnblogs.com/blog/1271254/202201/1271254-20220114144628010-640482062.png)

#### 2.安全实现手段

##### 1.统一安全认证和权限管理

由于hadoop本身没什么安全机制，Hadoop集群安全，首先就会想到业界通用的解决方案： Kerberos。Kerberos是一种网络认证协议，其设计目标是通过密钥系统为客户机/服务器应用程序提供强大的认证服务。该认证过程的实现不依赖于主机操作系统的认证，不需要基于主机地址的信任，不要求网络上所有主机的物理安全，并假定网络上传送的数据包可以被任意读取、修改和插入数据。

Kerberos通常会与LDAP配合使用。在大数据平台通常服务器多、租户也较多，需要进行Linux层面及应用层面的统一，这也就是构建Kerberos+LDAP这一组合的缘由。LDAP是一个轻量级的产品，作为一个统一认证的解决方案，其主要优点在于能够快速响应用户的查找需求。

除了统一认证，在数据的传输过程中，可以通过选择适合的SSL（Secure Socket Layer）证书，对传输中的一些敏感数据进行加密。 SSL证书可加密隐私数据，使黑客无法截取到用户敏感信息的明文数据，因此部署SSL证书是网络安全的基础防护措施之一。一份SSL证书包括一个公共密钥和一个私用密钥。公共密钥用于加密信息，私用密钥用于解译加密的信息。当用户端的浏览器指向一个安全域时，SSL同步确认服务器和客户端，并创建一种加密方式和一个唯一的会话密钥。它们可以启动一个保证消息的隐私性和完整性的安全会话。

在数据的操作和应用过程中，可以通过权限管理，控制不同的角色能操作的数据权限。设计良好的大数据平台权限管理，能从两个维度控制角色权限：第一个维度是控制粒度，如控制到字段级权限，第二个维度控制动作，如控制该角色是否能进行select、alter、delete等操作。

##### 2.资源隔离

在资源隔离层面，可以通过建立不同的租户，对不同权限的数据资源进行隔离。多租户技术是一种软件架构技术，可实现在多用户环境下共用相同的系统或程序组件，并且可确保各用户间数据的隔离性。多租户在数据存储上存在三种主要的方案，按照隔离程度从高到低，分别是：

- 独立数据库
- 共享数据库，隔离数据架构
- 共享数据库，共享数据架构

##### 3.数据加密

数据加密是用某种特殊的算法改变原有的信息数据使其不可读或无意义，使未授权用户获得加密后的信息，因不知解密的方法仍无法了解信息的内容。

先进行数据资产安全分类分级，然后对不同类型和安全等级的数据指定不同的加密要求和加密强度。尤其是大数据资产中非结构化数据涉及文档、图像和声音等多种类型，其加密等级和加密实现技术不尽相同，因此，需要针对不同的数据类型提供快速加解密技术。

根据数据是否流动的特点，数据加密分为存储加密和传输加密。

##### 4.数据脱敏

为了防止用户隐私信息、商业机密信息和企业内部数据泄露，在数据的传输、共享、展现等环节，往往需要对数据中台中的某些敏感数据进行脱敏操作。

大数据脱敏主要包括以下两大功能：

**1.敏感数据识别**

通过设置敏感数据的发现机制，计算机自动识别敏感数据，并在发现敏感数据后自动为该敏感数据打上相应的标签。

- 建立敏感数据规则：建立敏感信息样本库，定义企业的敏感信息的具体特征。比如：身份证号码、手机号码、生日、信用卡号码等。
- 敏感数据检测：脱敏系统支持对大数据平台存储的结构化和半结构化数据库、表进行敏感数据扫描探测，并对每个数据表进行抽样数据匹配，基于敏感信息库来检测存储在大数据平台的敏感数据，如客户信息、交易数据等。脱敏系统将数据库中包含敏感信息的表和字段标记出来以实现各类高级数据安全功能。例如：利用敏感数据标记实现以下自定义规则：只向外传输姓名，不是信息泄露事件；姓名、账号和电话等信息同时向外泄露，则认定为信息泄露事件。

**2.敏感数据脱敏**

提供敏感数据的动态脱敏功能，保障敏感数据访问安全。同时基于大数据安全分析技术，发现访问敏感数据的异常行为，并在可能的情况下进行追踪。最常见的脱敏方式包括如下几种形式：

- 数据替换：以虚构数据代替数据的真实值。
- 截断、加密、隐藏或使之无效：以“无效”或“*****”等代替数据的真实值。
- 随机化：以随机数据代替数据的真实值。
- 偏移：通过随机移位改变数字型的数据。

##### 5.数据共享安全

数据对外共享一般包括两种方式：**接口和文件**。

- **接口方式**包括接口数据（JSON/XML）、流式数据（Kafka）等多种数据访问方式。通过API操作权限管理、API流量管控、API认证管理等手段实现接口管控。
- **文件方式**主要指通过FTP、SFTP、邮件等对外共享数据，数据类型包括TXT、CSV、Word、PPT、Excel、HTML等，通过数字暗水印进行安全防护。数字暗水印通过对共享的文件嵌入暗水印作为标记一起传输，保障数据在发生泄露时，能够提取水印信息并追踪至责任人，达到事后安全保护的目的，解决了数据泄露后无法追踪、难以定责、难以避免再发生的问题。

##### 6.数据的容灾备份

服务器的硬件故障、软件故障、网络发生问题等，都可能导致数据丢失、错误或损坏。另外，人为的操作失误、自然灾害、战争等不可预料的因素，也可能导致发生不可挽回的数据丢失，给用户带来巨大损失。

为了应对这些情况，用户必须考虑数据的容灾备份，确保在任何情况下都不会影响到重要业务活动的持续开展。用户可以根据恢复目标将业务的关键等级划分为核心业务系统、一般性重要业务系统和一般业务系统三个级别，并根据不同级别分别有针对性地制订容灾备份方案。

##### 7.日志审计

日志审计系统是用于全面收集企业IT系统中常见的安全设备、网络设备、数据库、服务器、应用系统、主机等设备所产生的日志（包括运行、告警、操作、消息、状态等）并进行存储、监控、审计、分析、报警、响应和报告的系统。