- [数据中台与传统大数据平台有什么区别？_一个写湿的程序猿的博客-CSDN博客](https://qinjl.blog.csdn.net/article/details/118565639)

导读：我们可以这样理解，传统大数据平台和[数据仓库](https://so.csdn.net/so/search?q=数据仓库&spm=1001.2101.3001.7020)是数据中台的数据来源，建设数据中台是为了更好地服务于业务部门。

下图显示了信息化系统、数据仓库、传统大数据平台、数据中台之间的关系，其中的箭头表示数据的主要流向

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210708084053361.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMyNzI3MDk1,size_16,color_FFFFFF,t_70)

数据中台与传统大数据平台到底有什么区别？

为了叙述方便，我们先给出传统大数据平台的架构入下图。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2021070808420975.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMyNzI3MDk1,size_16,color_FFFFFF,t_70)

- 大数据基础能力层：[Hadoop](https://so.csdn.net/so/search?q=Hadoop&spm=1001.2101.3001.7020)、Spark、Flink、Hive、HBase、Flume、Sqoop、Kafka等。
- 在大数据组件上搭建的 ETL流水线，包括数据分析、机器学习程序。
- 数据治理系统。
- 数据仓库系统。
- 数据可视化系统。

可以看到，这些是传统大数据平台的核心功能。

在很多大数据项目里，只要把这些系统搭起来，每天可以生成业务报表（包括实时大屏），就算大数据平台搭建成功了。

但数据中台应该是大数据平台的一个超集。我们认为，在大数据平台的基础之上，数据中台还应该提供下面的系统功能。

1、全局的数据应用资产管理

这里所说的数据应用资产管理包括整个生态系统中的数据和应用。

传统的数据资产管理绝大部分只包括关系型数据库中的资产（包括Hive）,而一个数据中台应该管理所有结构化、非结构化的数据资产，以及使用这些数据资产的应用。

如果传统的数据资产管理提供的是数据目录，那么数据中台提供的应该是扩展的数据及应用目录。

要避免重复造轮子，首先要知道系统中有哪些轮子，因此维护一个系统中数据及数据应用的列表是很关键的。

2、全局的数据治理机制

与传统的数据治理不一样，数据中台必须提供针对全局的数据治理工具和机制。

传统数据仓库中的数据建模和数据治理大多针对一个特定部门的业务，部分原因是全局数据建模和治理周期太长，由于存在部门之间的协调问题，往往难度很大。

数据中台提供的数据治理机制必须允许各个业务部门自主迭代，但前提是要有全局一致的标准。阿里提出的`OneID`强调全局统一的对象ID（例如用户ID），就属于这个机制。

3、自助的、多租户的数据应用开发及发布

现有的绝大部分大数据平台要求使用者具备一定的编程能力。数据中台强调的是为业务部门赋能，而业务人员需要有一个自助的、可适应不同水平和能力要求的开发平台。

这个开发平台要能够保证数据隔离和资源隔离，这样任何一个使用系统的人都不用担心自己会对系统造成损害。

4、数据应用运维

用户应该可以很方便地将自己开发的数据应用自助发布到生产系统中，而无须经过专门的数据团队。

因为我们需要共享这些应用及其产生的数据，所以需要有类似于CI/CD的专门系统来管理应用的代码质量和进行版本控制。

在数据应用运行过程中产生的数据也需要全程监控，以保证数据的完整性、正确性和实时性

5、数据应用集成

应该可以随时集成新的数据应用。

新的大数据应用、人工智能工具不断涌现，我们的系统应该能够随时支持这些新应用。如果数据中台不能支持这些应用，各个业务部门可能又会打造自己的小集群，造成新的数据孤岛及应用孤岛。

6、数据即服务，模型即服务

数据分析的结果，不管是统计分析的结果，还是机器学习生成的模型，应该能够很快地使用无代码的方式发布，并供全机构使用。

7、数据能力共享管理

大部分数据能力应当具有`完善的共享管理机制`、`方便安全的共享机制`、`灵活的反馈机制`。

最后决定数据如何使用的是独立的个人，他们需要一套获取信息的机制，因此在机构内部必须要有这样的共享机制，才能真正让数据用起来。

8、完善的运营指标

数据中台强调的是可衡量的数据价值，因此，对于数据在系统中的使用方式、被使用的频率、最后产生的效果，必须要有一定的运营指标，才能验证数据的价值和数据中台项目的效率。

综合上面的讨论，除了阿里巴巴提出的`OneID`、`OneModel`、`OneService`之外，我们认为数据中台还应该满足以下两个要求。

1、TotalPlatform

所有中台数据及相关的应用应该在统一平台中统一管理。

如果有数据存储在中台管理不到的地方，或者有人在中台未知的情况下使用数据，我们就无法真正实现对数据的全局管理。

这要求数据中台能快速支持新的数据格式和数据应用，便于数据工具的共享，而无须建立一个分离的系统。

2、TotalInsight

数据中台应该能够理解并管理系统中数据的流动，提供数据价值的定量衡量，明确各个部门的花费和产出。

整个中台的运营是有序可控的，而不是一个黑盒子，用户可以轻松理解全局的数据资产和能力，从系统中快速实现数据变现。

如下图所示，数据中台可以说是按照一定的规范要求建设的数据能力平台，在`数据仓库`、`大数据平台`、`数据服务`、`数据应用`的建设中实现了符合`OneID`、`OneModel`、`OneService`的数据层。

这个数据层，加上在其上建立的业务能力层以及运营这个数据中台需要的`TotalPlatform`、`TotalInsight`，形成我们看到的数据中台。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210708090658254.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMyNzI3MDk1,size_16,color_FFFFFF,t_70)